package bio;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javafx.embed.swing.SwingFXUtils;
import javafx.stage.Stage;
import javafx.scene.control.TextField;

import javax.imageio.ImageIO;

public class Controller implements Initializable {

    public static final int RED = 0;
    public static final int GREEN = 1;
    public static final int BLUE = 2;

    public static final String LOW_PASS = "Low pass";
    public static final String SOBEL = "Sobel";
    public static final String PREWITT = "Prewitt";
    public static final String LAPLACE = "Laplace";
    public static final String CORNER = "Corner";

    private ObservableList<String> convFilters = FXCollections.observableArrayList(LOW_PASS, SOBEL, PREWITT, LAPLACE, CORNER);

    @FXML
    ImageView orginalImageView;
    @FXML
    ImageView editableImageView;
    @FXML
    Slider scaleSlider;
    @FXML
    TextField redTextField;
    @FXML
    TextField greenTextField;
    @FXML
    TextField blueTextField;
    @FXML
    TextField stretchStartValue;
    @FXML
    TextField stretchEndValue;
    @FXML
    Label currentRGB;
    @FXML
    Label currentBrightness;
    @FXML
    TextField customThreshold;
    @FXML
    TextField binWindowSize;
    @FXML
    TextField thresholdParameter;

    @FXML
    private BarChart redHistogram;

    @FXML
    private BarChart greenHistogram;

    @FXML
    private BarChart blueHistogram;

    @FXML
    private BarChart rgbHistogram;

    @FXML
    private ComboBox comboConvFilters;

    @FXML
    private GridPane gridPane;

    private BufferedImage bufferedImage;

    private int[] rgbArray;

    private Image image;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        handleSizeImageChanged();
        handlePixelCheckRgb();
        handlePixelClick();
        redTextField.setText(String.valueOf(0));
        greenTextField.setText(String.valueOf(0));
        blueTextField.setText(String.valueOf(0));
        currentRGB.setText("Current rgb: (,,,)");
        comboConvFilters.setValue("");
        comboConvFilters.setItems(convFilters);
        handleChooseConvFilters();

    }

    public void handleChooseConvFilters() {

        comboConvFilters.setOnAction((event) -> {
            String selected = (String) comboConvFilters.getSelectionModel().getSelectedItem();
            switch (selected) {
                case LOW_PASS:
                    processConvolution(FiltersMask.LOW_PASS);
                    updateAllHistograms();
                    break;
                case SOBEL:
                    processConvolution(FiltersMask.SOBEL);
                    updateAllHistograms();
                    break;
                case PREWITT:
                    processConvolution(FiltersMask.PREWITT);
                    updateAllHistograms();
                    break;
                case LAPLACE:
                    processConvolution(FiltersMask.LAPLACE);
                    updateAllHistograms();
                    break;
                case CORNER:
                    processConvolution(FiltersMask.CORNER);
                    updateAllHistograms();
                    break;
            }
        });
    }

    public void handleKuwaharaFilter() {
        int[][][] area1, area2, area3, area4; //(x,y,color)->value
        int[][] mean = new int[4][3];//(area number, color)->mean
        int[][] variance = new int[4][3];//(area number, color)->variance
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        for (int x = 2; x < width - 2; x++) {
            for (int y = 2; y < height - 2; y++) {

                area1 = createRGBArea(x - 2, y - 2, 3);
                area2 = createRGBArea(x - 2, y, 3);
                area3 = createRGBArea(x, y - 2, 3);
                area4 = createRGBArea(x, y, 3);
                mean[0] = processAreaMean(area1);
                mean[1] = processAreaMean(area2);
                mean[2] = processAreaMean(area3);
                mean[3] = processAreaMean(area4);
                variance[0] = processAreaVariance(area1, mean[0]);
                variance[1] = processAreaVariance(area2, mean[1]);
                variance[2] = processAreaVariance(area3, mean[2]);
                variance[3] = processAreaVariance(area4, mean[3]);

                pixelWriter.setColor(x, y, Color.rgb(mean[findMeanForColor(variance, RED)][RED], mean[findMeanForColor(variance, GREEN)][GREEN], mean[findMeanForColor(variance, BLUE)][BLUE]));
            }
        }
        setImage(writableImage);
        updateAllHistograms();

    }

    private int findMeanForColor(int[][] variance, int color) {
        int index = 0;
        int minVariance = variance[index][color];

        for (int i = 0; i < 4; i++) {
            if (variance[i][color] < minVariance) {
                minVariance = variance[i][color];
                index = i;
            }
        }
        return index;
    }

    private int[][][] createRGBArea(int startX, int startY, int n) {

        int[][][] area = new int[n][n][3];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                area[i][j] = getColorsArray(image, startX + i, startY + j);
            }
        }
        return area;
    }

    private int[][] createAreaRGBForMedian(int startX, int startY, int n) {

        int[][] area = new int[3][n * n];
        int[] originalColors;
        int k = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                originalColors = getColorsArray(image, startX + i, startY + j);
                area[RED][k] = originalColors[RED];
                area[GREEN][k] = originalColors[GREEN];
                area[BLUE][k] = originalColors[BLUE];
                k++;
            }
        }
        return area;
    }

    private int[] processAreaMean(int[][][] area) {
        int[] mean = new int[3];
        int sumRed = 0, sumGreen = 0, sumBlue = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sumRed += area[i][j][RED];
                sumGreen += area[i][j][GREEN];
                sumBlue += area[i][j][BLUE];
            }
        }
        mean[RED] = sumRed / 9;
        mean[GREEN] = sumGreen / 9;
        mean[BLUE] = sumBlue / 9;

        return mean;
    }

    private int[] processAreaVariance(int[][][] area, int[] mean) {
        int[] variance = new int[3];
        int sumRed = 0, sumGreen = 0, sumBlue = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sumRed += Math.pow(area[i][j][RED] - mean[RED], 2);
                sumGreen += Math.pow(area[i][j][GREEN] - mean[GREEN], 2);
                sumBlue += Math.pow(area[i][j][BLUE] - mean[BLUE], 2);
            }
        }

        variance[RED] = sumRed / 9;
        variance[GREEN] = sumGreen / 9;
        variance[BLUE] = sumBlue / 9;

        return variance;
    }

    public void handleMedian3Filter() {
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();
        int[][] area;

        for (int x = 1; x < width - 1; x++) {
            for (int y = 1; y < height - 1; y++) {

                area = createAreaRGBForMedian(x - 1, y - 1, 3);
                pixelWriter.setColor(x, y, Color.rgb(medianFilter(area[RED]), medianFilter(area[GREEN]), medianFilter(area[BLUE])));
            }
        }
        setImage(writableImage);
        updateAllHistograms();

    }

    public void handleMedian5Filter() {
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();
        int[][] area;

        for (int x = 2; x < width - 2; x++) {
            for (int y = 2; y < height - 2; y++) {

                area = createAreaRGBForMedian(x - 2, y - 2, 5);
                pixelWriter.setColor(x, y, Color.rgb(medianFilter(area[RED]), medianFilter(area[GREEN]), medianFilter(area[BLUE])));
            }
        }
        setImage(writableImage);
        updateAllHistograms();

    }

    private int medianFilter(int[] values) {
        Arrays.sort(values);
        return values[values.length / 2];
    }


    public void handleOwnMaskFilter() {
        int[][] mask = getMask();
        processConvolution(mask);
        updateAllHistograms();

    }

    public void handleAddPointsToBrightness() {
        if (editableImageView.getImage() == null) return;
        changeBrightnessForImage(true);
        updateAllHistograms();
    }


    public void handleRemovePointsToBrightness() {
        if (editableImageView.getImage() == null) return;

        changeBrightnessForImage(false);
        updateAllHistograms();
    }

    public void handleEqualizeImage() {
        if (editableImageView.getImage() == null) return;
        equalizeImage();
        updateAllHistograms();

    }

    public void handleStretchImage() {
        if (stretchStartValue.getText().isEmpty() || stretchEndValue.getText().isEmpty()) return;
        stretchImageWithRange(Integer.parseInt(stretchStartValue.getText()), Integer.parseInt(stretchEndValue.getText()));
        updateAllHistograms();
    }

    public void handleUndoAllChanges() {
        WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);

        editableImageView.setImage(image);

        editableImageView.setFitWidth(image.getWidth());
        editableImageView.setFitHeight(image.getHeight());
        updateAllHistograms();
    }

    public void handleUploadImageClick() {
        bufferedImage = uploadImage();
        if (bufferedImage != null) {
            WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);
            WritableImage orginalImage = SwingFXUtils.toFXImage(bufferedImage, null);

            editableImageView.setImage(image);
            editableImageView.setFitWidth(image.getWidth());
            editableImageView.setFitHeight(image.getHeight());
            updateAllHistograms();

            orginalImageView.setImage(orginalImage);
            orginalImageView.setFitWidth(250);
            orginalImageView.setFitHeight(250);
        }
    }

    public void handleSaveImageClick() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilterAll = new FileChooser.ExtensionFilter("All Files", "*.jpg", "*.png", "*.bmp", "*.gif", "*.tiff", "*.tif");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("jpg", "*.jpg"),
                new FileChooser.ExtensionFilter("png", "*.png"),
                new FileChooser.ExtensionFilter("bmp", "*.bmp"),
                new FileChooser.ExtensionFilter("gif", "*.gif"),
                new FileChooser.ExtensionFilter("tiff", "*.tiff"),
                new FileChooser.ExtensionFilter("tif", "*.tif")

        );
        fileChooser.setTitle("Save Image");

        File file = fileChooser.showSaveDialog(new Stage());
        if (file != null) {
            try {
                String extension = file.getAbsolutePath();
                String parts[] = extension.split("/");
                String fileName = parts[parts.length - 1];
                String filenameParts[] = fileName.split(Pattern.quote("."));
                extension = filenameParts[filenameParts.length - 1];

                if (extension.matches("jp.?g")) {
                    BufferedImage sourceImage = SwingFXUtils.fromFXImage(this.image, null);
                    BufferedImage targetImage = new BufferedImage(
                            (int) editableImageView.getImage().getWidth(),
                            (int) editableImageView.getImage().getHeight(),
                            BufferedImage.TYPE_INT_RGB
                    );
                    Graphics2D graphics2D = targetImage.createGraphics();
                    graphics2D.drawImage(sourceImage, 0, 0, null);
                    ImageIO.write(targetImage, extension, file);
                } else {
                    ImageIO.write(SwingFXUtils.fromFXImage(editableImageView.getImage(),
                            null), extension, file);
                }

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void handleSizeImageChanged() {
        scaleSlider.valueChangingProperty().addListener((observable, oldValue, newValue) -> {

            if (image != null) {
                editableImageView.setFitWidth(image.getWidth() * scaleSlider.getValue());
                editableImageView.setFitHeight(image.getHeight() * scaleSlider.getValue());
            }
        });

    }

    public void handlePixelClick() {
        editableImageView.setOnMouseClicked(evt -> {
            int red = Integer.parseInt(redTextField.getText());
            int blue = Integer.parseInt(blueTextField.getText());
            int green = Integer.parseInt(greenTextField.getText());
            double scale = scaleSlider.getValue();

            WritableImage image = (WritableImage) editableImageView.getImage();

            PixelWriter pixelWriter = image.getPixelWriter();

            pixelWriter.setColor((int) (evt.getX() / scale), (int) (evt.getY() / scale), Color.color(red / 255, green / 255, blue / 255));
        });
    }

    public void handlePixelCheckRgb() {
        editableImageView.setOnMouseMoved(evt -> {
            // System.out.printf("coordinate X: %.2f, coordinate Y: %.2f\n", evt.getX(), evt.getY());
            if (editableImageView.getImage() == null) return;
            PixelReader pixelReader = editableImageView.getImage().getPixelReader();
            double scale = scaleSlider.getValue();
            Color color = pixelReader.getColor((int) (evt.getX() / scale), (int) (evt.getY() / scale));
            currentRGB.setText(String.format("Current rgb: (%d,%d,%d)", (int) (color.getRed() * 255), (int) (color.getGreen() * 255), (int) (color.getBlue() * 255)));
        });
    }

    public void handleBinarizationCustomThreshold() {
        if (editableImageView.getImage() == null || customThreshold.getText().isEmpty()) return;
        int threshold = Integer.parseInt(customThreshold.getText());
        doThresHoldBinarization(threshold);
        updateAllHistograms();
    }

    public void handleBinarizationOtsuMethod() {
        if (editableImageView.getImage() == null) return;
        int threshold = getThresholdOtsuMethod();
        doThresHoldBinarization(threshold);
        updateAllHistograms();
    }

    public void handleBinarizationNiblackMethod() {

        if (editableImageView.getImage() == null || binWindowSize.getText().isEmpty() || thresholdParameter.getText().isEmpty())
            return;

        binThresholdNiblackMethod(Integer.parseInt(binWindowSize.getText()), Float.parseFloat(thresholdParameter.getText()));
        updateAllHistograms();
    }


    private void setImage(WritableImage writableImage) {

        editableImageView.setImage(writableImage);
        editableImageView.setFitWidth(writableImage.getWidth());
        editableImageView.setFitHeight(writableImage.getHeight());
    }

    public int[][] getMask() {
        int[][] gridMask = new int[3][3];
        for (Node child : gridPane.getChildren()) {
            Integer column = GridPane.getColumnIndex(child);
            Integer row = GridPane.getRowIndex(child);
            if (column == null) column = 0;
            if (row == null) row = 0;
            gridMask[row][column] = Integer.valueOf(((TextField) child).getText());
        }
        return gridMask;
    }

    private int[] getColorsArray(Image image, int xpos, int ypos) {
        Color color = image.getPixelReader().getColor(xpos, ypos);
        int[] colorsRGB = new int[3];
        colorsRGB[0] = (int) (color.getRed() * 255);
        colorsRGB[1] = (int) (color.getGreen() * 255);
        colorsRGB[2] = (int) (color.getBlue() * 255);
        return colorsRGB;
    }

    private BufferedImage uploadImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Upload Image");

        FileChooser.ExtensionFilter extFilterAll = new FileChooser.ExtensionFilter("All Files", "*.jpg", "*.png", "*.bmp", "*.gif", "*.tiff", "*.tif");
        fileChooser.getExtensionFilters().addAll(extFilterAll);

        //Show open file dialog
        File file = fileChooser.showOpenDialog(null);
        if (file == null) return null;

        WritableImage image;
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            return bufferedImage;

        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private void setHistogram(int[] colorArray, BarChart histogram) {
        XYChart.Series series = new XYChart.Series();

        for (int i = 0; i < 256; i++) {
            series.getData().add(new XYChart.Data(String.valueOf(i), colorArray[i]));
        }
        histogram.getData().add(series);
    }

    private void updateAllHistograms() {

        redHistogram.getData().clear();
        blueHistogram.getData().clear();
        greenHistogram.getData().clear();
        rgbHistogram.getData().clear();

        redHistogram.setTitle("Red histogram");
        blueHistogram.setTitle("Blue histogram");
        greenHistogram.setTitle("Green histogram");
        rgbHistogram.setTitle("Average histogram");


        int[] redArray = new int[256];
        int[] greenArray = new int[256];
        int[] blueArray = new int[256];
        rgbArray = new int[256];

        image = editableImageView.getImage();
        int redValue, blueValue, greenValue;

        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                Color color = image.getPixelReader().getColor(i, j);
                redValue = (int) (color.getRed() * 255);
                greenValue = (int) (color.getGreen() * 255);
                blueValue = (int) (color.getBlue() * 255);
                redArray[redValue]++;
                greenArray[greenValue]++;
                blueArray[blueValue]++;
                rgbArray[(redValue + greenValue + blueValue) / 3]++;

            }
        }
        setHistogram(redArray, redHistogram);
        setHistogram(greenArray, greenHistogram);
        setHistogram(blueArray, blueHistogram);
        setHistogram(rgbArray, rgbHistogram);
    }

    private int findAverageRGBValue(Image image, int xpos, int ypos) {
        Color color = image.getPixelReader().getColor(xpos, ypos);
        int redValue = (int) (color.getRed() * 255);
        int greenValue = (int) (color.getGreen() * 255);
        int blueValue = (int) (color.getBlue() * 255);

        return (redValue + greenValue + blueValue) / 3;
    }

    private int findIndexMinValue(int[] array) {

        int min = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                min = i;
                break;
            }
        }
        return min;
    }

    private int findIndexMaxValue(int[] array) {

        int max = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            if (array[i] != 0) {
                max = i;
                break;
            }
        }
        return max;
    }

    private void stretchImageWithRange(int a, int b) {

        double[] lutArray = prepareLutTableToStretch(a, b);
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();

        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {

                pixelWriter.setColor(j, i, Color.grayRgb((int) lutArray[findAverageRGBValue(image, j, i)]));
            }
        }
        setImage(writableImage);
    }

    private double[] prepareLutTableToStretch(int a, int b) {

        double[] lut = new double[256];

        for (int i = 0; i < 256; i++) {
            if (i <= a || i >= b) rgbArray[i] = 0;
        }

        //g = gmax((f-fmin)/(fmax-fmin)), where f is intensity of input pixel, fmax(min) maximum value of pixel in given range

        int fmin = findIndexMinValue(rgbArray);
        int fmax = findIndexMaxValue(rgbArray);
        double newValuePixel;

        for (int i = 0; i < 256; i++) {
            if (fmax - fmin == 0) break;
            newValuePixel = 255 * (i - fmin) / (fmax - fmin);

            if (newValuePixel > 255) newValuePixel = 255;
            if (newValuePixel < 0) newValuePixel = 0;
            lut[i] = newValuePixel;
        }
        return lut;
    }


    private void changeBrightnessForImage(boolean bright) {

        double[] lutArray = prepareLutBrightness(bright);
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        int red, blue, green;

        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Color color = image.getPixelReader().getColor(i, j);

                red = (int) (color.getRed() * 255);
                green = (int) (color.getGreen() * 255);
                blue = (int) (color.getBlue() * 255);

                pixelWriter.setColor(i, j, Color.rgb((int) lutArray[red], (int) lutArray[green], (int) lutArray[blue]));
            }
        }

        setImage(writableImage);
    }


    private double[] prepareLutBrightness(boolean bright) {
        double[] lut = new double[256];
        int value;
        for (int i = 0; i < 256; i++) {


            if (bright) {
                value = (int) Math.sqrt((double) i * 255);

            } else {
                value = (int) (Math.pow((double) i / 255, 2) * 255);

            }
            if (value > 255) value = 255;
            if (value < 0) value = 0;

            lut[i] = value;
        }
        return lut;
    }


    private double[] prepareDistributionFunction(int[] colorArray) {

        //colorArray keep number of pixel for each color value

        double[] distribution = new double[256];
        double sum = 0;
        int countPixels = (int) image.getHeight() * (int) image.getWidth();
        for (int i = 0; i < 256; i++) {
            sum += colorArray[i];
            distribution[i] = sum / countPixels;
        }
        return distribution;
    }

    private double[] prepareLutEqualizing(int[] colorArray) {
        double[] distribution = prepareDistributionFunction(colorArray);
        double[] lut = new double[256];
        double value;

        for (int i = 0; i < 256; i++) {

            value = distribution[i] * 255;
            if (value > 255) value = 255;
            if (value < 0) value = 0;
            lut[i] = value;
        }

        return lut;
    }

    private void equalizeImage() {

        double[] lutArray = prepareLutEqualizing(rgbArray);
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();

        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {

                pixelWriter.setColor(j, i, Color.grayRgb((int) lutArray[findAverageRGBValue(image, j, i)]));
            }
        }
        setImage(writableImage);
    }


    private void convertToBlackAndWhite() {
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        PixelReader pixelReader = image.getPixelReader();
        double red;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                red = pixelReader.getColor(i, j).getRed() * 255;
                pixelWriter.setColor(i, j, Color.rgb((int) red, (int) red, (int) red));
            }
        }

        setImage(writableImage);

    }

    private void doThresHoldBinarization(int threshold) {

        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        PixelReader pixelReader = image.getPixelReader();
        double red;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                red = pixelReader.getColor(i, j).getRed() * 255;
                pixelWriter.setColor(i, j, Color.grayRgb((red < threshold) ? 0 : 255));
            }
        }

        setImage(writableImage);
    }


    private void doThresHoldBinarizationNiblack(int[][] niblack) {

        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        PixelReader pixelReader = image.getPixelReader();
        double red;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                red = pixelReader.getColor(i, j).getRed() * 255;
                pixelWriter.setColor(i, j, Color.grayRgb((red < niblack[i][j]) ? 0 : 255));
            }
        }
        setImage(writableImage);
    }

    private int getThresholdOtsuMethod() {
        int[] histogramArray = rgbArray;
        int numberOfPixels = (int) image.getWidth() * (int) image.getHeight();
        float sumBackground = 0;
        float sumForeground;
        float weightBackground, weightForeground, meanBackground, meanForeground, betweenClassVariance;
        float sumWithWeightBack = 0;
        float sumWithWeightAll = 0;

        float varMax = 0;
        int threshold = 0;

        for (int i = 0; i < 256; i++) sumWithWeightAll += i * histogramArray[i];

        for (int i = 0; i < 256; i++) {
            sumBackground += histogramArray[i];
            if (sumBackground == 0) continue;

            sumForeground = numberOfPixels - sumBackground;
            if (sumForeground == 0) break;

            weightBackground = sumBackground / numberOfPixels;
            weightForeground = sumForeground / numberOfPixels;

            sumWithWeightBack += (float) (i * histogramArray[i]);

            meanBackground = sumWithWeightBack / weightBackground;
            meanForeground = (sumWithWeightAll - sumWithWeightBack) / weightForeground;

            betweenClassVariance = weightBackground * weightForeground * (meanBackground - meanForeground) * (meanBackground - meanForeground);

            if (betweenClassVariance > varMax) {
                varMax = betweenClassVariance;
                threshold = i;
            }
        }

        return threshold;
    }

    private void binThresholdNiblackMethod(int winSize, float thresholdParameter) {
        int[][] niblackArray;

        niblackArray = getSingleThresholdNiblack(winSize, thresholdParameter);
        doThresHoldBinarizationNiblack(niblackArray);
    }

    private int[][] getSingleThresholdNiblack(int winSize, float thresholdParameter) {
        int[][] niblackArray = new int[(int) image.getWidth()][(int) image.getHeight()];

        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {

                int startX = x - winSize / 2;
                int startY = y - winSize / 2;

                int sum = 0;
                float sumDiff = 0;
                for (int i = startX; i < startX + winSize; i++) {
                    for (int j = startY; j < startY + winSize; j++) {
                        if (i < 0 || i >= image.getWidth() || j < 0 || j >= image.getHeight()) continue;
                        sum += checkRGBAverageColor(i, j);
                    }
                }

                float average = (float) sum / (float) (winSize * winSize);

                for (int i = startX; i < startX + winSize; i++) {

                    for (int j = startY; j < startY + winSize; j++) {
                        if (i < 0 || i >= image.getWidth() || j < 0 || j >= image.getHeight()) continue;
                        sumDiff += ((float) checkRGBAverageColor(i, j) - average) * ((float) checkRGBAverageColor(i, j) - average);
                    }
                }

                niblackArray[x][y] = (int) (average + thresholdParameter * (float) Math.sqrt((float) sumDiff / Math.pow(winSize, 2)));

            }
        }

        return niblackArray;
    }

    private int checkRGBAverageColor(int x, int y) {
        Color color = image.getPixelReader().getColor(x, y);
        return ((int) (color.getRed() * 255) + (int) (color.getGreen() * 255) + (int) (color.getBlue() * 255)) / 3;
    }


    private void processConvolution(int[][] convolutionMask) {
//check before that mask sum is not equal to 0
        int height = (int) editableImageView.getImage().getHeight();
        int width = (int) editableImageView.getImage().getWidth();
        WritableImage writableImage = new WritableImage(width, height);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        Image image = editableImageView.getImage();

        int sum = 0;
        //Sum mask
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sum += convolutionMask[i][j];
            }
        }
        System.out.println(sum);
        int kernelRed, kernelGreen, kernelBlue;
        int[] orginalColors;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                kernelBlue = kernelGreen = kernelRed = 0;
                for (int i = -1; i <= 1; i++) {
                    for (int j = -1; j <= 1; j++) {
                        if (x + i < 0 || x + i >= width || y + j < 0 || y + j >= height) continue;
                        orginalColors = getColorsArray(image, x + i, y + j);
                        kernelRed += convolutionMask[i + 1][j + 1] * orginalColors[RED];
                        kernelGreen += convolutionMask[i + 1][j + 1] * orginalColors[GREEN];
                        kernelBlue += convolutionMask[i + 1][j + 1] * orginalColors[BLUE];
                    }
                }
                pixelWriter.setColor(x, y, Color.rgb(prepareValueInRange(kernelRed, sum), prepareValueInRange(kernelGreen, sum), prepareValueInRange(kernelBlue, sum)));

            }
        }
        setImage(writableImage);
    }


    private int prepareValueInRange(int value, int sum) {
        if (sum != 0)
            value = value / sum;

        if (value > 255) return 255;
        else if (value < 0) return 0;
        else return value;
    }

    /**
     * FINGERPRINTS - THINNING
     */
    public void handleThinning() {
        if (editableImageView.getImage() == null) return;

        //1. Binarization
        int threshold = getThresholdOtsuMethod();
        if (threshold != 0) doThresHoldBinarization(threshold);

        //Add 0 for white, 1 for black
        Image image = editableImageView.getImage();
        int[][] imageArray = (new KMMAlgorithm()).processKMM(image);

        int width = (int) image.getWidth();
        int height = (int) image.getHeight();

        WritableImage writableImage = new WritableImage(width, height);

        PixelWriter writer = writableImage.getPixelWriter();

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (imageArray[i][j] == 0) {
                    writer.setColor(i, j, Color.WHITE);
                } else {
                    writer.setColor(i, j, Color.BLACK);
                }

            }
        }
        setImage(writableImage);
    }
}