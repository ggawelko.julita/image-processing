package bio;

public class FiltersMask {

    public static int[][] LOW_PASS = {
            {1, 2, 1},
            {2, 4, 2},
            {1, 2, 1},
    };

    public static int[][] SOBEL = {
            {-1, 0, 1},
            {-2, 0, 2},
            {-1, 0, 1}
    };

    public static int[][] PREWITT = {
            {-1, 0, 1},
            {-1, 0, 1},
            {-1, 0, 1}
    };

    public static int[][] LAPLACE = {
            {0, -1, 0},
            {-1, 4, -1},
            {0, -1, 0}
    };

    public static int[][] CORNER = {
            {1, 1, 1},
            {1, -2, -1},
            {1, -1, -1}
    };

}
