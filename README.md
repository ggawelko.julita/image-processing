# Image processing

Project was created for biometric classes. (4-6.2018)

Include basics actions like upload, save, check colour of pixel, zoom.

Other:

#### 1. filters 
* convolution filters ex. low pass, sobel
* kuwahara
* median filter for reduce salt-and-peper noise
#### 2. streching, change brightness, equalizing

#### 3. binarization
* own threshold
* Otsu method
* Niblack
#### 4. thinning KMM algorithm


### Thinning example:

![Thinning](/docs/thin.png)


